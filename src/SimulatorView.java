import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.HashMap;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.io.IOException;
import java.awt.Image;




/**
 * A graphical view of the simulation grid.
 * The view displays a colored rectangle for each location 
 * representing its contents. It uses a default background color.
 * Colors for each type of species can be defined using the
 * setColor method.
 * 
 * @author David J. Barnes and Michael Kolling
 * @version 2006.03.30
 * @modified Chuck Cusack, 2007.09.14
 * 
 */
// This class was a JFrame, but since we want to add buttons to
// be able to control it, it is now a JPanel instead.  This means
// A JFrame had to be placed in Simulator instead.
// 
public class SimulatorView extends JPanel
{
    // Colors used for empty locations.
    private static final Color EMPTY_COLOR = Color.green;

    // Color used for objects that have no defined color.
    private static final Color UNKNOWN_COLOR = Color.gray;

    private final String STEP_PREFIX = "Step: ";
    private final String POPULATION_PREFIX = "Population: ";
    private JLabel stepLabel, population;
    private FieldView fieldView;
    
    // A map for storing colors for participants in the simulation
    private HashMap<Class, Color> colors;
    // A statistics object computing and storing simulation information
    private FieldStats stats;
    //Import image 
    private Image rabbitPic, foxPic, disasterPic, volcanoPic;

    /**
     * Create a view of the given width and height.
     * @param height The simulation's height.
     * @param width  The simulation's width.
     */
    public SimulatorView(int height, int width)
    {
        stats = new FieldStats();
        colors = new HashMap<Class, Color>();
        
        stepLabel = new JLabel(STEP_PREFIX, JLabel.CENTER);
        population = new JLabel(POPULATION_PREFIX, JLabel.CENTER);
   
        fieldView = new FieldView(height, width);
        
        // Set the layout manager for this JPanel
        this.setLayout(new BorderLayout());
        
        // Add the labels and view of the field to this panel
        this.add(stepLabel, BorderLayout.NORTH);
        this.add(fieldView, BorderLayout.CENTER);
        this.add(population, BorderLayout.SOUTH);
        
      //Import image into the program
        try {
			rabbitPic = ImageIO.read(this.getClass().getResource("rabbit.png"));
			foxPic = ImageIO.read(this.getClass().getResource("fox.jpg"));
			disasterPic = ImageIO.read(this.getClass().getResource("disaster.png"));
			volcanoPic = ImageIO.read(this.getClass().getResource("volcano.png"));
		} catch (IOException e) {

		}
    }
    
    /**
     * Define a color to be used for a given class of actor.
     * @param actorClass The actor's Class object.
     * @param color The color to be used for the given class.
     */
    public void setColor(Class actorClass, Color color)
    {
        colors.put(actorClass, color);
    }

    /**
     * @return The color to be used for a given class of actor.
     */
    private Color getColor(Class actorClass)
    {
        Color col = colors.get(actorClass);
        if(col == null) {
            // no color defined for this class
            return UNKNOWN_COLOR;
        }
        else {
            return col;
        }
    }

    /**
     * Show the current status of the field.
     * @param step Which iteration step it is.
     * @param field The field whose status is to be displayed.
     */
    public void showStatus(int step, Field field)
    {
        if(!isVisible())
            setVisible(true);
            
        stepLabel.setText(STEP_PREFIX + step);
        stats.reset();
        
        fieldView.preparePaint();

        for(int row = 0; row < field.getDepth(); row++) {
            for(int col = 0; col < field.getWidth(); col++) {
                Actor actor = field.getActorAt(row, col);
                if(actor != null) {
                    stats.incrementCount(actor.getClass());
                    fieldView.drawMark(col, row, actor);
                }
                else {
                    fieldView.drawMark(col, row, null);
                }
            }
        }
        stats.countFinished();

        population.setText(POPULATION_PREFIX + stats.getPopulationDetails(field));
        fieldView.repaint();
    }

    /**
     * Determine whether the simulation should continue to run.
     * @return true If there is more than one species alive.
     */
    public boolean isViable(Field field)
    {
        return stats.isViable(field);
    }
    
    /**
     * Provide a graphical view of a rectangular field. This is 
     * a nested class (a class defined inside a class) which
     * defines a custom component for the user interface. This
     * component displays the field.
     * This is rather advanced GUI stuff - you can ignore this 
     * for your project if you like.
     */
    private class FieldView extends JPanel
    {
        private final int GRID_VIEW_SCALING_FACTOR = 6;

        private int gridWidth, gridHeight;
        private int xScale, yScale;
        Dimension size;
        private Graphics g;
        private Image fieldImage;

        /**
         * Create a new FieldView component.
         */
        public FieldView(int height, int width)
        {
            gridHeight = height;
            gridWidth = width;
            size = new Dimension(0, 0); 
        }

        /**
         * Tell the GUI manager how big we would like to be.
         */
        public Dimension getPreferredSize()
        {
            return new Dimension(gridWidth * GRID_VIEW_SCALING_FACTOR,
                                 gridHeight * GRID_VIEW_SCALING_FACTOR);
        }

        /**
         * Prepare for a new round of painting. Since the component
         * may be resized, compute the scaling factor again.
         */
        public void preparePaint()
        {
            if(! size.equals(getSize())) {  // if the size has changed...
                size = getSize();
                fieldImage = fieldView.createImage(size.width, size.height);
                g = fieldImage.getGraphics();

                xScale = size.width / gridWidth;
                if(xScale < 1) {
                    xScale = GRID_VIEW_SCALING_FACTOR;
                }
                yScale = size.height / gridHeight;
                if(yScale < 1) {
                    yScale = GRID_VIEW_SCALING_FACTOR;
                }
            }
        }
        
        /**
         * Paint on grid location on this field with a image of the object.
         */
        public void drawMark(int x, int y, Actor actor)
        {
        	if(actor == null) {
        		g.setColor(EMPTY_COLOR);
        		g.fillRect(x * xScale, y * yScale, xScale-1, yScale-1);
        	}
        	else {
        		if(actor instanceof Rabbit) {
        			g.drawImage(rabbitPic,x * xScale, y * yScale, xScale-1, yScale-1,null);
        		}
        		else if(actor instanceof Fox) {
        			g.drawImage(foxPic,x * xScale, y * yScale, xScale-2, yScale-2, null);
        		}
        		else if(actor instanceof Disaster) {
        			g.drawImage(disasterPic,x * xScale, y * yScale, xScale-1, yScale-1,null);
        		}
        		else if(actor instanceof Volcano) {
        			g.drawImage(volcanoPic,x * xScale, y * yScale, xScale-1, yScale-1,null);
        		}
        	}
        }

        /**
         * The field view component needs to be redisplayed. Copy the
         * internal image to screen.
         */
        public void paintComponent(Graphics g)
        {
            if(fieldImage != null) {
                Dimension currentSize = getSize();
                if(size.equals(currentSize)) {
                    g.drawImage(fieldImage, 0, 0, null);
                }
                else {
                    // Rescale the previous image.
                    g.drawImage(fieldImage, 0, 0, currentSize.width, currentSize.height, null);
                }
            }
        }
    }
}
