import java.util.Random;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Collections;
import java.awt.Color;

// Added the following lines
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * A simple predator-prey simulator, based on a field containing rabbits and
 * foxes.
 * 
 * @author David J. Barnes and Michael Kolling
 * @version 2006.03.30
 * @modified Chuck Cusack, September, 2007
 * 
 */
public class Simulator {
	// Constants representing configuration information for the simulation.
	// The default width for the grid.
	private static final int DEFAULT_WIDTH = 50;
	// The default depth of the grid.
	private static final int DEFAULT_DEPTH = 50;
	// The probability that a fox will be created in any given grid position.
	private static final double FOX_CREATION_PROBABILITY = 0.02;
	// The probability that a rabbit will be created in any given grid position.
	private static final double RABBIT_CREATION_PROBABILITY = 0.08;
	// The probability that a rabbit will be created in any given grid position.
	private static final double DISASTER_CREATION_PROBABILITY = 0.005;
	// The probability that a rabbit will be created in any given grid position.
	private static final double VOLCANO_CREATION_PROBABILITY = 0.003;

	// The current state of the field.
	private Field field;
	// A second field, used to build the next stage of the simulation.
	private Field updatedField;
	// The current step of the simulation.
	private int step;
	// A graphical view of the simulation.
	private SimulatorView view;

	// Added this variable for use by the thread.
	private int numberSteps;

	// Added the following GUI stuff
	// The main window to display the simulation (and your buttons, etc.).
	JButton runOneButton;
	JButton run100StepButton;
	JButton runLongButton;
	JButton resetButton;
	JTextField tf;
	JTextField widthInput;
	JTextField depthInput;
	JButton simulateButton;
	JButton customize;
	private JFrame mainFrame;

	/**
	 * Construct a simulation field with default size.
	 */
	public Simulator() {
		this(DEFAULT_DEPTH, DEFAULT_WIDTH);
	}

	public static void main(String[] args) {
		// Create the simulator
		Simulator s = new Simulator();
	}

	/**
	 * Create a simulation field with the given size.
	 * 
	 * @param depth
	 *            Depth of the field. Must be greater than zero.
	 * @param width
	 *            Width of the field. Must be greater than zero.
	 */
	public Simulator(int depth, int width) {
		if (width <= 0 || depth <= 0) {
			System.out.println("The dimensions must be greater than zero.");
			System.out.println("Using default values.");
			depth = DEFAULT_DEPTH;
			width = DEFAULT_WIDTH;
		}
		field = new Field(depth, width);
		updatedField = new Field(depth, width);

		// Create a view of the state of each location in the field.
		view = new SimulatorView(depth, width);
		//view.setColor(Rabbit.class, Color.orange);
		//view.setColor(Fox.class, Color.blue);
		//view.setColor(Disaster.class, Color.green);

		// The rest of this method has changed. This sets up the
		// JFrame to display everything.

		mainFrame = new JFrame();

		mainFrame.setTitle("Fox and Rabbit Simulation");

		// Add window listener so it closes properly.
		// when the "X" in the upper right corner is clicked.
		mainFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		// Add menu bar to the frame
		makeMenuBar(mainFrame);

		// Add toolbar to the frame
		makeToolBar();
		// Adding things to a JFrame first requires that you get the
		// content pane. Notice you don't do with with a JPanel.
		Container contents = mainFrame.getContentPane();
		contents.add(view, BorderLayout.CENTER);

		// You always need to call pack on a JFrame.
		mainFrame.pack();

		// For very subtle reasons, this MUST go after the pack statement above.
		// Just trust me on this one. Or figure out why for yourself.
		reset();

		// You always need to call setVisible(true) on a JFrame.
		mainFrame.setVisible(true);
	}

	/**
	 * Run the simulation from its current state for a reasonably long period, e.g.
	 * 500 steps.
	 */
	public void runLongSimulation() {
		simulate(500);
	}

	/**
	 * Run the simulation from its current state for the given number of steps. Stop
	 * before the given number of steps if it ceases to be viable. This has been
	 * modified so it uses a thread--this allows it to work in conjunction with
	 * Swing. Modified by Chuck Cusack, Sept 18, 2007
	 * 
	 * @param numSteps
	 *            How many steps to run for.
	 */
	public void simulate(int numSteps) {
		// For technical reason, I had to add numberSteps as a class variable.
		numberSteps = numSteps;
		// Create a thread
		Thread runThread = new Thread() {
			// When the thread runs, it will simulate numberSteps steps.
			public void run() {
				// Disable the button until the simulation is done.
				changeButton(false);
				for (int step = 1; step <= numberSteps && view.isViable(field); step++) {
					simulateOneStep();
				}
				// Now re-enable the button
				changeButton(true);
			}
		};
		// Start the thread
		runThread.start();
		// Now this method exits, allowing the GUI to update. The simulation is being
		// run on a different thread, so the GUI updates as the simulation continues.
	}

	/**
	 * Run the simulation from its current state for a single step. Iterate over the
	 * whole field updating the state of each fox and rabbit.
	 */
	public void simulateOneStep() {
		step++;

		// let all actors act
		ArrayList<Actor> actors = field.getActors();
		Collections.shuffle(actors); // to randomize the order they act.
		for (Iterator<Actor> it = actors.iterator(); it.hasNext();) {
			Actor actor = it.next();
			actor.act(field, updatedField);
		}

		// Swap the field and updatedField at the end of the step.
		Field temp = field;
		field = updatedField;
		updatedField = temp;
		updatedField.clear();

		// Display the new field on screen.
		view.showStatus(step, field);
	}

	/**
	 * Reset the simulation to a starting position.
	 */
	public void reset() {
		step = 0;
		field.clear();
		updatedField.clear();
		populate(field);

		// Show the starting state in the view.
		view.showStatus(step, field);
	}

	/**
	 * Populate a field with foxes and rabbits.
	 * 
	 * @param field
	 *            The field to be populated.
	 */
	private void populate(Field field) {
		Random rand = new Random();
		field.clear();
		for (int row = 0; row < field.getDepth(); row++) {
			for (int col = 0; col < field.getWidth(); col++) {
				if (rand.nextDouble() <= FOX_CREATION_PROBABILITY) {
					Fox fox = new Fox(true);
					fox.setLocation(row, col);
					field.place(fox);
				} else if (rand.nextDouble() <= RABBIT_CREATION_PROBABILITY) {
					Rabbit rabbit = new Rabbit(true);
					rabbit.setLocation(row, col);
					field.place(rabbit);
				} else if (rand.nextDouble() <= DISASTER_CREATION_PROBABILITY) {
					Disaster disaster = new Disaster();
					disaster.setLocation(row, col);
					field.place(disaster);
				} else if (rand.nextDouble() <= VOLCANO_CREATION_PROBABILITY) {
					Volcano volcano = new Volcano();
					volcano.setLocation(row, col);
					field.place(volcano);
				}
				// else leave the location empty.
			}
		}
	}

	/**
	 * Create the main frame's menu bar.
	 * 
	 * @param frame
	 *            The frame that the menu bar should be added to.
	 */
	private void makeMenuBar(JFrame frame) {
		JMenuBar menubar = new JMenuBar();
		frame.setJMenuBar(menubar);
		JMenu menu;
		JMenuItem item;

		menu = new JMenu("File");
		menubar.add(menu);

		item = new JMenuItem("Exit");
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		menu.add(item);

		menu = new JMenu("Help");
		menubar.add(menu);
		item = new JMenuItem("About");
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showAbout();
			}
		});
		menu.add(item);
	}

	/**
	 * Method that open a small window with information
	 */
	private void showAbout() {
		JOptionPane panel = new JOptionPane();
		panel.showMessageDialog(null, "Wrote by Yizhe Zhang");
	}

	/**
	 * Create bar for all buttons
	 */
	private void makeToolBar() {
		Container contents = mainFrame.getContentPane();
		JPanel toolbar = new JPanel();
		toolbar.setLayout(new GridLayout(0, 1));

		// Add a button to run 1 steps.
		runOneButton = new JButton("Run 1 step");
		runOneButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				simulateOneStep();
			}
		});
		toolbar.add(runOneButton);

		// Add a button to run 100 steps.
		run100StepButton = new JButton("Run 100 steps");
		run100StepButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				simulate(100);
			}
		});
		toolbar.add(run100StepButton);

		// Add a button to run long steps.
		runLongButton = new JButton("Run 100 steps");
		runLongButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				runLongSimulation();
			}
		});
		toolbar.add(runLongButton);

		// Add a button to reset simulator.
		resetButton = new JButton("Reset");
		resetButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				reset();
			}
		});
		toolbar.add(resetButton);

		// Add a label hint input for number of step desired for textField below
		JLabel steps = new JLabel("Step you want and hit simulate", JLabel.CENTER);
		toolbar.add(steps);

		// Add textField of simulate steps to tool bar
		tf = new JTextField();
		toolbar.add(tf);

		// Add button for simulate certain step
		simulateButton = new JButton("Simulate");
		simulateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				runCertainStep();
			}
		});
		toolbar.add(simulateButton);

		// Add toolbar into panel with flow layout for spacing
		JPanel flow = new JPanel();
		flow.add(toolbar);
		contents.add(flow, BorderLayout.WEST);
	}

	/**
	 * Run certain step as user input
	 */
	private void runCertainStep() {
		try {
			String i = tf.getText();
			int x = Integer.parseInt(i);
			if (x < 0) {
				throw new Exception();
			}
			simulate(x);
		} catch (Exception e) {
			JOptionPane panel = new JOptionPane();
			panel.showMessageDialog(null, "please provide proper input");
		}
	}

	/**
	 * Disable or enable all button
	 */
	private void changeButton(boolean b) {
		runOneButton.setEnabled(b);
		run100StepButton.setEnabled(b);
		runLongButton.setEnabled(b);
		resetButton.setEnabled(b);
		simulateButton.setEnabled(b);
	}
}
