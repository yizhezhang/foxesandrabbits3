
public abstract class Actor {
	// The actor's location
	private Location location;

	/**
	 * Make this Actor act - that is: make it do whatever it wants/needs to do.
	 * 
	 * @param currentField
	 *            The field currently occupied.
	 * @param updatedField
	 *            The field to transfer to.
	 */
	abstract public void act(Field currentField, Field updatedField);

	/**
	 * Return the Actor's location.
	 * 
	 * @return The Actor's location.
	 */
	public Location getLocation() {
		return location;
	}

	/**
	 * Set the Actor's location.
	 * 
	 * @param row
	 *            The vertical coordinate of the location.
	 * @param col
	 *            The horizontal coordinate of the location.
	 */
	public void setLocation(int row, int col) {
		this.location = new Location(row, col);
	}

	/**
	 * Set the Actor's location.
	 * 
	 * @param location
	 *            The animal's location.
	 */
	public void setLocation(Location location) {
		this.location = location;
	}
}
