import java.util.Iterator;
import java.util.Random;

/**
 * A simple model for nature disaster A nature disaster exist for ever and move
 * around.
 * 
 * @author zyz62
 *
 */
public class Disaster extends Actor {
	private Random rand = new Random();

	/**
	 * Constructor for disaster a disaster do not age or die Nothing need to
	 * initialized all in super class actor A disaster do not age or die
	 */
	public Disaster() {

	}

	/**
	 * This is what disaster do, kill everything around it then jump to another
	 * location Two disaster meet together will become 1
	 */
	public void act(Field currentField, Field updatedField) {
		kill(currentField, getLocation());
		int y = currentField.getDepth();
		int x = currentField.getWidth();
		this.setLocation(rand.nextInt(x), rand.nextInt(y));
		updatedField.place(this); // sets location
	}

	// The method for disaster kill things around
	private void kill(Field field, Location location) {
		Iterator<Location> adjacentLocations = field.adjacentLocations(location);
		while (adjacentLocations.hasNext()) {
			Location where = adjacentLocations.next();
			Actor actor = field.getActorAt(where);
			if (actor instanceof Animal) {
				if (actor instanceof Rabbit) {
					Rabbit rabbit = (Rabbit) actor;
					if (rabbit.isAlive()) {
						rabbit.setDead();
					}
				}
				if (actor instanceof Fox) {
					Fox fox = (Fox) actor;
					if (fox.isAlive()) {
						fox.setDead();
					}
				}
			}
		}
	}
}
