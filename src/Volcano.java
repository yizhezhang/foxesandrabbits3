
public class Volcano extends Actor {
	private int count;

	/**
	 * Constructor of Volcano, only count need to be initialize here, everything
	 * else is done by superclass Volcano do not age or die
	 */
	public Volcano() {
		count = 0;
	}

	/**
	 * Method for the character to act once What exactly the method do is implement
	 * in another method
	 * 
	 * @param currentField
	 *            The field currently occupied.
	 * @param updatedField
	 *            The field to transfer to.
	 */
	public void act(Field currentField, Field updatedField) {
		expand(currentField, getLocation(), updatedField);
		kill(currentField, getLocation());
	}

	/**
	 * This is what a volcano do, expand in size and kill everything in its way
	 * 
	 * @param field
	 *            is the current field
	 * @param location
	 *            is the current location
	 * @param updatedField
	 *            is the field to transfer to
	 */
	public void expand(Field field, Location location, Field updatedField) {
		int x = location.getRow();
		int y = location.getCol();
		if (x < (field.getWidth() - 1) && y < (field.getDepth() - 1)) {
			Volcano volcano1 = new Volcano();
			volcano1.setLocation(x + 1, y + 1);
			updatedField.place(volcano1);
			Volcano volcano2 = new Volcano();
			volcano2.setLocation(x + 1, y);
			updatedField.place(volcano2);
			Volcano volcano3 = new Volcano();
			volcano3.setLocation(x, y + 1);
			updatedField.place(volcano3);
			Volcano volcano4 = new Volcano();
			volcano1.setLocation(x, y);
		}
	}

	/**
	 * The method that is responsible for kill animal in the place of volcano
	 * 
	 * @param currentField
	 *            The field currently occupied.
	 * @param location
	 *            is current location of volcano.
	 */
	public void kill(Field field, Location location) {
		int x = location.getRow();
		int y = location.getCol();
		if (x < (field.getWidth() - 1) && y < (field.getDepth() - 1)) {
			int xi = 0;
			while (xi < 2) {
				int yi = 0;
				while (yi < 2) {
					Actor actor = field.getActorAt(x + xi, y + yi);
					if (actor instanceof Animal) {
						if (actor instanceof Rabbit) {
							Rabbit rabbit = (Rabbit) actor;
							if (rabbit.isAlive()) {
								rabbit.setDead();
							}
						}
						if (actor instanceof Fox) {
							Fox fox = (Fox) actor;
							if (fox.isAlive()) {
								fox.setDead();
							}
						}
					}
					yi++;
				}
				xi++;
			}

		}
	}
}
